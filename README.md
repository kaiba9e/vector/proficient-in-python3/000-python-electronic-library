# Python Electronic Library

A Python electronic library of mine, which stores some classic e-books.

* 01 - [Learning Python  5th Edition](classic/01-LearningPython-5thEdition-2013.pdf)
  * [Learning Python     4th Edition](classic/01-LearningPython-5thEdition-2009.pdf)
* 02 - [Python Cookbook  3rd Edition](classic/02-PythonCookbook-3rdEdition-2013.pdf)
* 03 - [Effective Python 2nd Edition](classic/03-EffectivePython-2ndEdition-2020.pdf)
* 04 - [Fluent Python    1st Edition](classic/04-FluentPython-1stEdition-2015.pdf)
